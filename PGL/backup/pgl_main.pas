unit pgl_main;

{$mode objfpc}{$H+}

interface
uses
  Classes, SysUtils, crt;

procedure pgl_init(scr_x_size, scr_y_size, col_sym, col_bak:byte);
procedure pgl_draw_block(sx,sy,ex,ey:integer; sym:char; col_sym, col_bak:byte);
procedure pgl_draw_quad(sx,sy,ex,ey:integer; sym:char; col_sym, col_bak:byte);
procedure pgl_draw_border(sym_ul, sym_ur, sym_dl, sym_dr, sym_main: char; col_sym, col_bak:byte);
procedure pgl_draw_line(sx,sy,len:integer; sym:char; stepto:string; col_sym, col_bak:byte);
procedure pgl_draw_pixel(sx,sy: integer; col: byte);
procedure pgl_write_text(sx,sy:integer; stepto, str:string; col_sym, col_bak:byte);
procedure pgl_screen_update();
procedure pgl_screen_clear();


var
  i,j, screen_size_x, screen_size_y:integer;
  buf_screen_char: array[0..1200, 0..1200] of char;
  buf_screen_color: array[0..1200, 0..1200, 0..1] of byte;
implementation
    //Initialization procedure
    procedure pgl_init(scr_x_size, scr_y_size, col_sym, col_bak:byte);
    begin
        writeln('START');
        for i:=1 to scr_y_size do
        begin
          for j:=1 to scr_x_size do
          begin
            buf_screen_char[i,j]:=' ';
            buf_screen_color[i,j,0]:=col_sym;
            buf_screen_color[i,j,1]:=col_bak;
          end;
        end;
        screen_size_x:=scr_x_size;
        screen_size_y:=scr_y_size;
        writeln('SUCCESS');
    end;

    //Block draw procedure
    procedure pgl_draw_block(sx,sy,ex,ey:integer; sym:char; col_sym, col_bak:byte);
    begin
        for i:=sy to ey do
        begin
          for j:=sx to ex do
          begin
               buf_screen_char[i,j]:=sym;
               buf_screen_color[i,j,0]:=col_sym;
               buf_screen_color[i,j,1]:=col_bak;
          end;
          writeln;
        end;
    end;

    //Quad draw procedure
    procedure pgl_draw_quad(sx, sy, ex, ey: integer; sym: char; col_sym, col_bak: byte);
    begin
         for i:=sy to ey do
         begin
              for j:=sx to ex do
              begin
                   if ((i=sy) or (i=ey) or (j=sx) or (j=ex)) then
                   begin
                        buf_screen_char[i,j]:=sym;
                        buf_screen_color[i,j,0]:=col_sym;
                        buf_screen_color[i,j,1]:=col_bak;
                   end;
              end;
              writeln;
         end;
    end;

    //Borders draw procedure
    procedure pgl_draw_border(sym_ul, sym_ur, sym_dl, sym_dr, sym_main: char;
      col_sym, col_bak: byte);
    begin
         for i:=0 to screen_size_y do
         begin
              for j:=0 to screen_size_x do
              begin
                   if ((i=0) and (j=0)) then
                   begin
                        buf_screen_char[i,j]:=sym_ul;
                        buf_screen_color[i,j,0]:=col_sym;
                        buf_screen_color[i,j,1]:=col_bak;
                   end else
                   if ((i=0) and (j=screen_size_x)) then
                   begin
                        buf_screen_char[i,j]:=sym_ur;
                        buf_screen_color[i,j,0]:=col_sym;
                        buf_screen_color[i,j,1]:=col_bak;
                   end else
                   if ((i=screen_size_y) and (j=screen_size_x)) then
                   begin
                        buf_screen_char[i,j]:=sym_dr;
                        buf_screen_color[i,j,0]:=col_sym;
                        buf_screen_color[i,j,1]:=col_bak;
                   end else
                   if ((i=screen_size_y) and (j=0)) then
                   begin
                        buf_screen_char[i,j]:=sym_dl;
                        buf_screen_color[i,j,0]:=col_sym;
                        buf_screen_color[i,j,1]:=col_bak;
                   end else
                   if ((i=screen_size_y) or (i=0) or (j=screen_size_x) or (j=0)) then
                   begin
                        buf_screen_char[i,j]:=sym_main;
                        buf_screen_color[i,j,0]:=col_sym;
                        buf_screen_color[i,j,1]:=col_bak;
                   end;
              end;
         end;
    end;

    //Line draw procedure
        procedure pgl_draw_line(sx, sy, len: integer; sym: char; stepto: string;
      col_sym, col_bak: byte);
    begin
         case stepto of
         'up':
            begin
                 for j:=sy downto sy-len do
                 begin
                      buf_screen_char[j,sx]:=sym;
                      buf_screen_color[j,sx,0]:=col_sym;
                      buf_screen_color[j,sx,1]:=col_bak;
                 end;
            end;
         'down':
              begin
                   for j:=sy to sy+len do
                   begin
                        buf_screen_char[j,sx]:=sym;
                        buf_screen_color[j,sx,0]:=col_sym;
                        buf_screen_color[j,sx,1]:=col_bak;
                   end;
              end;
         'left':
              begin
                   for j:=sx downto sx-len do
                   begin
                        buf_screen_char[sy,j]:=sym;
                        buf_screen_color[sy,j,0]:=col_sym;
                        buf_screen_color[sy,j,1]:=col_bak;
                   end;
              end;
         'right':
               begin
                    for j:=sx to sx+len do
                    begin
                         buf_screen_char[sy,j]:=sym;
                         buf_screen_color[sy,j,0]:=col_sym;
                         buf_screen_color[sy,j,1]:=col_bak;
                    end;
               end;
         end;
    end;

    //Pixel draw procedure
    procedure pgl_draw_pixel(sx, sy: integer; col: byte);
    begin
         buf_screen_char[sy,sx]:=' ';
         buf_screen_color[]:=col;
    end;

    //Text writing procedure
    procedure pgl_write_text(sx, sy: integer; stepto, str: string; col_sym,
      col_bak: byte);
    var
       s: integer;
    begin
         s:=1;
         case stepto of
         'right':
                 begin
                      for j:=sx to (length(str)+sx-1) do
                      begin
                           buf_screen_char[sy,j]:=str[s];
                           buf_screen_color[sy,j,0]:=col_sym;
                           buf_screen_color[sy,j,1]:=col_bak;
                           s:=s+1;
                      end;
                 end;
         'left':
                begin
                     for j:=sx downto (sx-length(str)+1) do
                     begin
                          buf_screen_char[sy,j]:=str[s];
                          buf_screen_color[sy,j,0]:=col_sym;
                          buf_screen_color[sy,j,1]:=col_bak;
                          s:=s+1;
                     end;
                end;
         'up':
                begin
                     for j:=sy downto (sy-length(str)+1) do
                     begin
                          buf_screen_char[j,sx]:=str[s];
                          buf_screen_color[j,sx,0]:=col_sym;
                          buf_screen_color[j,sx,1]:=col_bak;
                          s:=s+1;
                     end;
                end;
         'down':
                begin
                     for j:=sy to (length(str)+sy-1) do
                     begin
                          buf_screen_char[j,sx]:=str[s];
                          buf_screen_color[j,sx,0]:=col_sym;
                          buf_screen_color[j,sx,1]:=col_bak;
                          s:=s+1;
                     end;
                end;
         end;
    end;

    //Screen update procedure
    procedure pgl_screen_update();
    begin
         clrscr;
         for i:=0 to screen_size_y do
         begin
              for j:=0 to screen_size_x do
              begin
                   textcolor(buf_screen_color[i,j,0]);
                   textbackground(buf_screen_color[i,j,1]);
                   write(buf_screen_char[i,j]);
              end;
              writeln;
         end;
    end;

    //Screen clean procedure
    procedure pgl_screen_clear();
    begin
        for i:=0 to screen_size_y do
        begin
             for j:=0 to screen_size_x do
             begin
                  buf_screen_char[i,j]:=' ';
             end;
        end;
    end;


end.

